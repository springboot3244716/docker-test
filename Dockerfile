# Paso 1: Utiliza una imagen base con Gradle y Java 21
FROM gradle:8.7.0-jdk21 AS build

WORKDIR /app

# Copiar archivos build.gradle y src al directorio de trabajo
COPY build.gradle settings.gradle ./
COPY src ./src

# Compilar el proyecto y empaquetar el JAR
RUN gradle clean build -x test --no-daemon

# Segunda etapa
FROM gradle:8.7.0-jdk21

# Copiar el archivo JAR generado en la etapa anterior al directorio de trabajo en la segunda etapa
COPY --from=build /app/build/libs/test-0.0.1-SNAPSHOT.jar /usr/test-0.0.1-SNAPSHOT.jar

# Establecer el directorio de trabajo en /usr
WORKDIR /usr

# Comando para ejecutar la aplicación al iniciar el contenedor
ENTRYPOINT ["java", "-jar", "test-0.0.1-SNAPSHOT.jar"]
